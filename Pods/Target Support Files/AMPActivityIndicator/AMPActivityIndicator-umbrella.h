#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "AMPActivityIndicator.h"

FOUNDATION_EXPORT double AMPActivityIndicatorVersionNumber;
FOUNDATION_EXPORT const unsigned char AMPActivityIndicatorVersionString[];

