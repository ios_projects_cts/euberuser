//
//  WalletViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/28/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    
    
    
    //MARK:- declare var,objects and outlets
    @IBOutlet weak var tbl_paymentModelist: UITableView!
    var paymentModeArr = ["Paypal","Credit/Debit Card","Cash"]
    var paymentModeImageArr = ["home","payment","history"]
    
    
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Wallet"
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = ""
        tbl_paymentModelist.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tbl_paymentModelist.backgroundColor = UIColor.clear
    }
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return paymentModeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell",
            for: indexPath)
        
        cell.textLabel?.text = paymentModeArr[indexPath.row]
        cell.imageView?.image = UIImage (named: paymentModeImageArr[indexPath.row])
        let itemSize:CGSize = CGSize(width:20,height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        let imageRect : CGRect = CGRect(x:0, y:0,width: itemSize.width, height:itemSize.height)
        cell.imageView!.image?.draw(in: imageRect)
        cell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //  cell.title_name?.text = vehicle_typeArr[indexPath.row]
        
        
        return cell
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
