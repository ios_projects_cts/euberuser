//
//  CancelRideViewController.swift
//  Euber
//
//  Created by chawtech solutions on 3/1/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class CancelRideViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- declare var,objects and outlets
     var tbl_cancelreasonslist: UITableView  =   UITableView()
     @IBOutlet weak var txtv_cancelreasonsTitle: UITextView!
    
    var cancelreasonsArr = ["Driver issue","Arrival time issue","Route issue","Others"]
    //var paymentModeImageArr = ["home","payment","history"]
    var activityIndicatorView: ActivityIndicatorView!

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
       
        self.title = "Cancel"

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = ""
        
        
        //TODO:- create tableview programatically
        tbl_cancelreasonslist = UITableView(frame: (CGRect (x:10, y:100 , width: self.view.frame.size.width-20, height: self.view.frame.size.height/3)), style: UITableViewStyle.plain)
        tbl_cancelreasonslist.delegate      =   self
        tbl_cancelreasonslist.dataSource    =   self
        tbl_cancelreasonslist.isHidden = true
        tbl_cancelreasonslist.separatorStyle = .none
        
        txtv_cancelreasonsTitle.backgroundColor = UIColor.clear
        tbl_cancelreasonslist.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.view.addSubview(tbl_cancelreasonslist)
       
        
        
        // crete navigation br button
        let btn_doneRide = UIButton(type: .custom)
        // btn_menu.setImage(UIImage(named: "menu_icon"), for: .normal)
        btn_doneRide.setTitle("Done", for: .normal)
        btn_doneRide.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        btn_doneRide.addTarget(self, action: #selector(CancelRideViewController.donebuttonAction), for: .touchUpInside)
        let doneRide_btn = UIBarButtonItem(customView: btn_doneRide)
        self.navigationItem.setRightBarButtonItems([doneRide_btn], animated: true)
        
        
        
//        // TODO:- set tap gesture recognizer
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeTableview(_:)))
//        self.view.addGestureRecognizer(tapGesture)
    }
    
//    //MARK:- remove Keyboard action of tapgesture
//    func removeTableview(_ sender: UITapGestureRecognizer) {
//        
//        tbl_cancelreasonslist.isHidden = true
//    }

    //MARK:- menu button Action
    func donebuttonAction()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        self.callCancelCabMethod()
    }
    
    // MARK:- btn_dropdown Action
    @IBAction func btn_dropdownAction(_ sender: Any)
    {
        tbl_cancelreasonslist.isHidden = false

    }
   
    
  
    //MARK:- server call of Cancel booking method
    func callCancelCabMethod()
    {
        DispatchQueue.main.async
            {
                self.activityIndicatorView.startAnimating()
        }
        
        
        let apiCall = JsonApi()
        let parameters = [
            
            "booking_id": UserDefaults.standard.object(forKey:"booking_id")!,
            "message": txtv_cancelreasonsTitle.text!
            ]
        
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalcancelBookingUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                
                print(status)
                if success == "true"
                {
                    
                    DispatchQueue.main.async
                        {
                            
                            self.activityIndicatorView.stopAnimating()
                            
                            // create the alert
                            let alert = UIAlertController(title:"Message", message:message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                
                               
                            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                            if (controller is HomeViewController) {
                            _ =  self.navigationController?.popToViewController(controller, animated: false)
                            }
                            }

                                        
                                        
                                
                                
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                    }
                }
                else
                {
                    DispatchQueue.main.async
                        { self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                }
            }
            else
            {
                
            }
        }
        
    }

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cancelreasonsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell",
            for: indexPath)
        
        cell.textLabel?.text = cancelreasonsArr[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
    
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tbl_cancelreasonslist.isHidden = true
        txtv_cancelreasonsTitle.text = nil
        if cancelreasonsArr[indexPath.row] == "Others"
        {
            txtv_cancelreasonsTitle.becomeFirstResponder()
            txtv_cancelreasonsTitle.backgroundColor = UIColor.white
        }
        else
        {
             txtv_cancelreasonsTitle.resignFirstResponder()
            txtv_cancelreasonsTitle.text = cancelreasonsArr[indexPath.row]
            txtv_cancelreasonsTitle.backgroundColor = UIColor.clear
            txtv_cancelreasonsTitle.isUserInteractionEnabled = false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
