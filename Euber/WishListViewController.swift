//
//  WishListViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/14/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class WishListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK:- declare var,objects and outlets
    @IBOutlet weak var tbl_wishlist: UITableView!
 // @IBOutlet weak var txtf_search: UITextField!
    var getFavoritesListArr = NSMutableArray()
    var activityIndicatorView: ActivityIndicatorView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
         self.callgetFavouritesMethod()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       //txtf_search.clearButtonMode = .whileEditing
        
       
        
//        activityIndicatorView = ActivityIndicatorView(title: "Processing...", center: self.view.center)
//        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
    }
   
     //MARK:- btn cancel action
     @IBAction func btn_CancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func callgetFavouritesMethod()
    {
        
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": UserDefaults.standard.object(forKey: "passengerID")!
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetFavouritesUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let status = result["status"] as! String
            if status == "success"
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // parse data from server
                    let venderArr=result["data"] as? [[String: Any]]
                    self.parseGetFavouritesListData(dataArray: venderArr!)
                    
                }
                else
                {
                    // display alert controller
                    self.displayAlertWithMessage(viewController:self, title: message, message:"")
                }
            }
            else
            {
                
            }
            
        }
    }
    
    
    //  MARK:- parseGetLocationListData
    func parseGetFavouritesListData(dataArray:[[String: Any]])
    {
        getFavoritesListArr.removeAllObjects()
        
        let favoriteDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            favoriteDict.setValue((dataArray[i]["address"] as? NSString), forKey: "address")
            favoriteDict.setValue((dataArray[i]["lat"] as? NSString), forKey: "lat")
            favoriteDict.setValue((dataArray[i]["lng"] as? NSString), forKey: "lng")
            favoriteDict.setValue((dataArray[i]["locationid"] as? NSString), forKey: "locationid")
               favoriteDict.setValue((dataArray[i]["locationtype"] as? NSString), forKey: "locationtype")
            getFavoritesListArr.add(favoriteDict.mutableCopy() )
            
            
        }
        DispatchQueue.main.sync
            {
                
                tbl_wishlist.reloadData()
                self.activityIndicatorView.stopAnimating()
        }
        
    }
    // MARK:- btn_wishList Action
    @IBAction func btn_newWishlistAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "NewWishlistViewController") as! NewWishlistViewController
        self.present(vc, animated: true, completion: nil)
    }

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return getFavoritesListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! WishListTVCell
        
        let dict = getFavoritesListArr[indexPath.row] as? Dictionary<String,String>
        cell.lbl_title.text = dict?["locationtype"]
        cell.lbl_subtitle.text = dict?["address"]
          //  cell.title_name?.text = vehicle_typeArr[indexPath.row]
        
        
                return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = getFavoritesListArr[indexPath.row] as? Dictionary<String,String>
        
        self.dismiss(animated: true, completion: nil)
        
        let imageDataDict:[String: String] = ["address": (dict?["address"])!]
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "favouriteprovider"), object: nil, userInfo: imageDataDict)
        
        
        
        
    }

    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//               return 40
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
