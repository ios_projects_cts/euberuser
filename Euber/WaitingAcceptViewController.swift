//
//  WaitingAcceptViewController.swift
//  Euber
//
//  Created by chawtech solutions on 3/1/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import AMPActivityIndicator

class WaitingAcceptViewController: UIViewController ,CLLocationManagerDelegate{
 @IBOutlet var spinner: AMPActivityIndicator!
    @IBOutlet weak var mapView : GMSMapView!
 var activityIndicatorView: ActivityIndicatorView!
    var locationManager = CLLocationManager()
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
         self.navigationItem.setHidesBackButton(true, animated:false)
        //self.title = "Wait"
        
        
        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        
        self.spinner.barWidth = 20
        self.spinner.startAnimating()
    }
    
  


func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    //print("Error" + error.description)
}

func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
{
    let userLocation = locations.last
//    let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
    
    let camera = GMSCameraPosition.camera(withLatitude: (userLocation?.coordinate.latitude)!,
            longitude:(userLocation?.coordinate.longitude)!, zoom: 14)
    // mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-40), camera: camera)
    mapView.camera = camera
    locationManager.stopUpdatingLocation()
    
//    let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2DMake((userLocation?.coordinate.latitude)!, (userLocation?.coordinate.longitude)!);
//    let circ = GMSCircle(position: circleCenter, radius: 500)
//    circ.fillColor = UIColor.init(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
//    circ.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
//    circ.strokeWidth = 0;
//    circ.map = self.mapView;
}
    // handle notification
    func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"] as! [String:AnyObject]
        print(data)
        
        let notification =  data["notificaton_type"]as? String
        let notification_type = notification?.replacingOccurrences(of:"\"", with:"")
        
        // let notification_type = String(describing:data["notificaton_type"])
        if notification_type! == "Ride Completed."
        {
            
        }
        if notification_type! == "Booking Confirmed."
        {
            
            // create the alert
            let alert = UIAlertController(title:"Message", message:"Bokking Confirmed", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
                    vc.userInfo = data
                    // self.navigationController?.pushViewController(vc, animated: false)
                    self.present(vc, animated: true, completion: nil)
                }
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backItem?.title = ""
        
        
        //user location stuff
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        
        // crete navigation br button
        let btn_cancelRide = UIButton(type: .custom)
       // btn_menu.setImage(UIImage(named: "menu_icon"), for: .normal)
        btn_cancelRide.setTitle("Cancel", for: .normal)
        btn_cancelRide.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btn_cancelRide.addTarget(self, action: #selector(WaitingAcceptViewController.cancelbuttonAction), for: .touchUpInside)
        let cancelRide_btn = UIBarButtonItem(customView: btn_cancelRide)
        self.navigationItem.setRightBarButtonItems([cancelRide_btn], animated: true)

       
    }
    
    //MARK:- menu button Action
    func cancelbuttonAction()
    {
    
        // call next view controller in main queue
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier: "CancelRideViewController") as! CancelRideViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }


    }
    
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
