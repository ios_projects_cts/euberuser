//
//  SignUpViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/13/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate {
    
 //MARK:- declare var,objects and outlets
    @IBOutlet weak var txtf_pass: UITextField!
    @IBOutlet weak var txtf_email: UITextField!
    @IBOutlet weak var txtf_phone: UITextField!
    @IBOutlet weak var txtf_name: UITextField!
     @IBOutlet weak var scrollView: UIScrollView!
     var activeField: UITextField!
    
     //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
          scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)

        //TODO:-  set tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard(_:)))
        self.scrollView.addGestureRecognizer(tapGesture)
        
        //TODO:-  set keyboard notification center
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK:- textfield delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    //MARK:- keyboard notification methods
    func keyboardWillShow(notification: NSNotification) {
        
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+200)
        
        
        if activeField != nil {
            
        //    scrollView.isScrollEnabled = false
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0{
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
              //  scrollView.isScrollEnabled = false
            }
        }
    }
    
    //MARK:- remove Keyboard action of tapgesture
    func removeKeyboard(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }


    @IBAction func btn_signupAction(_ sender: Any) {
        if ((txtf_email.text?.characters.count)! > 0)
            && ((txtf_pass.text?.characters.count)! > 0)
            && ((txtf_name.text?.characters.count)! > 0)
            && ((txtf_phone.text?.characters.count)! > 0)
        
        {
            if  isValidEmail(testStr:txtf_email.text!)  == true
            {
                
                    
                if Reachability.isConnectedToNetwork() == true
                {
                    print("Internet Connection Available!")
                    // call register method
                    self.callRegisterMethod()
                }
                else
                {
                    print("Internet Connection not Available!")
                    DispatchQueue.main.async {
                        self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Please check your network connection")
                    }
                }

                
            }
            else
            {
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Email is not in correct format")
                }
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Field can't be empty")
            }
            
        }
    }
    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    @IBAction func btn_backtoHomeAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: false)
    }

    // MARK:- Server call of register Method
    func callRegisterMethod()
    {
        
        let apiCall = JsonApi()
        let parameters = [
            
            "password": txtf_pass.text!,
            "email": txtf_email.text!,
            "name": txtf_name.text!,
            "phone": txtf_phone.text!,
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalRegisterUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // call pop view controller in main queue
                    DispatchQueue.main.async
                        {
                            _ = self.navigationController?.popViewController(animated: false)
                            
                    }
                }
                else
                {
                    // call alert controller
                    DispatchQueue.main.async
                        {
                            self.displayAlertWithMessage(viewController: self, title: "Alert!", message:message)
                    }
                }
                
            }
            else
            {
                
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
