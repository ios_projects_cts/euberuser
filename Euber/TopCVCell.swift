//
//  TopCVCell.swift
//  Euber
//
//  Created by chawtech solutions on 2/14/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class TopCVCell: UICollectionViewCell {
     @IBOutlet weak var view_Back: UIView!
    @IBOutlet weak var lbl_tripClassName: UILabel!
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //        //MARK:- set effect of uiview for search
        
                self.view_Back.backgroundColor = UIColor.white
                self.view_Back.layer.cornerRadius = 3.0
                self.view_Back.layer.borderWidth = 2.0
                self.view_Back.layer.borderColor = UIColor.clear.cgColor
                self.view_Back.layer.shadowColor = UIColor.lightGray.cgColor
                self.view_Back.layer.shadowOpacity = 1.0
                self.view_Back.layer.shadowRadius = 1.0
                self.view_Back.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))
    }
}
