//
//  MapRouteViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/18/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapRouteViewController: UIViewController,CLLocationManagerDelegate {

  //  @IBOutlet weak var driver_distance: UILabel!
  //  @IBOutlet weak var driver_long: UILabel!
    @IBOutlet weak var driver_carNo: UILabel!
    @IBOutlet weak var driver_mob: UILabel!
    @IBOutlet weak var driver_name: UILabel!
      @IBOutlet weak var view_driverInfo: UIView!
    var userInfo = [String:AnyObject]()
   var activityIndicatorView: ActivityIndicatorView!
     var timer = Timer()
    
    //MARK:- Declaration of var and objects
    var locationManager:CLLocationManager!
    var current_latitude = String()
    var current_longitude = String()
    var mapView : GMSMapView!
    var desti_longitude = Double()
    var desti_latitude = Double()
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userInfo)
        navigationController?.navigationBar.backItem?.title = ""
        
        // self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        print(userInfo)
        
        //  let data = userInfo["data_driver"] as(AnyObject)
       // print(data)
        
        driver_name.text = userInfo["driver_name"] as? String
        driver_mob.text = userInfo["driver_mobile"] as? String
        driver_carNo.text = userInfo["driver_LicenseNo"] as? String
       // driver_distance.text = userInfo["drop_location"] as? String
        
        desti_latitude = Double(((userInfo["driver_lat"])! as? String)!)!
        desti_longitude = Double(((userInfo["driver_long"])! as? String)!)!
        
        driver_carNo.layer.borderWidth = 0.5
        driver_carNo.layer.borderColor = UIColor.gray.cgColor
        driver_carNo.layer.cornerRadius = 2
        driver_carNo.clipsToBounds = true
        
     //    call determine loction method
        self.determineCurrentLocation()
        
        // TODO:- set tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.callDriverInfoAction(_:)))
        self.view_driverInfo.addGestureRecognizer(tapGesture)
        
       //  self.timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        
    }
    
    
    // called every time interval from the timer
    func timerAction()
    {
        print("timer")
        
        self.determineCurrentLocation()
        
    }

    //MARK:- remove Keyboard action of tapgesture
    func callDriverInfoAction(_ sender: UITapGestureRecognizer) {
        
        // call next view controller in main queue
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier: "DriverAcceptViewController") as! DriverAcceptViewController
           self.present(vc, animated: true, completion:nil)
        }
    }
    
    
    //MARK:- determine Current Location for user
    func determineCurrentLocation()
    {
        
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())

        DispatchQueue.main.async
        {
            self.activityIndicatorView.startAnimating()
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        //        if CLLocationManager.locationServicesEnabled() {
        //            //locationManager.startUpdatingHeading()
        //            locationManager.startUpdatingLocation()
        //        }
    }
    
    
    //MARK:- Core Location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // let userLocation:CLLocation = locations[0] as CLLocation
        guard let userLocation = locations.first else { return }
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        manager.stopUpdatingLocation()
        
        current_latitude = String(userLocation.coordinate.latitude)
        current_longitude = String(userLocation.coordinate.longitude)
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude,longitude:  userLocation.coordinate.longitude, zoom: 16)
        mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-80), camera: camera)
        mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView) //= mapView
        
        // TODO:- get formatted address of current user usimng geo location
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            // Print each key-value pair in a new row
            addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
                let userAddress = formattedAddress.joined(separator: ", ")
                
                let marker = GMSMarker()
                marker.position = center
                marker.title = (addressDict["Name"] as? String)!
                marker.snippet = userAddress
               // marker.icon = UIImage(named:"driver-car")
                marker.map = self.mapView
                
            }
            // TODO:- showing location of other user
            let position = CLLocationCoordinate2D(latitude:self.desti_latitude, longitude: self.desti_latitude)
            let marker = GMSMarker(position: position)
            marker.title = "Hello World"
            marker.map = self.mapView
            marker.icon = UIImage(named:"vender_icon")
            
            // TODO:- call function to draw route from source to destination using google direction api
            self.drawPathFromSourcetoDestination()
            
            //            // Access each element manually
            //            if let locationName = addressDict["Name"] as? String {
            //                print(locationName)
            //            }
            //            // Access each element manually
            //            if let locationstreet = addressDict["Street"] as? String {
            //                print(locationstreet)
            //            }
            //            if let street = addressDict["Thoroughfare"] as? String {
            //                print(street)
            //            }
            //            if let city = addressDict["City"] as? String {
            //                print(city)
            //            }
            //            if let zip = addressDict["ZIP"] as? String {
            //                print(zip)
            //            }
            //            if let country = addressDict["Country"] as? String {
            //                print(country)
            //            }
            
        })
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    //MARK:- server call function to draw route from source to destination using google direction api
    func drawPathFromSourcetoDestination ()
    {
        
        let para:NSMutableDictionary = NSMutableDictionary()
        
        let origin = "\(current_latitude),\(current_longitude)"
      //  let destination = "\(desti_latitude),\(desti_longitude)"
        let destination = "\(desti_latitude),\(desti_longitude)"
        
        
        //create the url with NSURL
        let urlValue = NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving")
        // print(urlValue ?? default value)
        //create the session obje
        let session = URLSession.shared
        
        //now create the NSMutableRequest object using the url object
        let request = NSMutableURLRequest(url: urlValue! as URL)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: para, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as? [String:Any] {
                    print(json)
                    
                    
                    // TODO:- showing polyline for route in google map
                    let routes = (json["routes"] as! Array<Dictionary<String, AnyObject>>)[0]
                    let  routeOverviewPolyline = routes["overview_polyline"] as! Dictionary<String, AnyObject>
                    let points = routeOverviewPolyline["points"]
                    
                    
                    DispatchQueue.main.async {
                       
                        self.activityIndicatorView.stopAnimating()
                        let path = GMSPath.init(fromEncodedPath: points! as! String)
                        let polyline = GMSPolyline.init(path: path)
                        polyline.map = self.mapView
                        // polyline.strokeColor = UIColor .black
                        
                        
//                        let path1 = GMSMutablePath()
//                        //Change coordinates
//                        path1.add(CLLocationCoordinate2D(latitude: Double( self.current_latitude)!, longitude:Double(self.current_longitude)!))
//                     //   path1.add(CLLocationCoordinate2D(latitude: 28.538848, longitude: 77.395678))
//                     //   path1.add(CLLocationCoordinate2D(latitude: 28.538540, longitude: 77.396128))
//                        let polyline1 = GMSPolyline(path: path1)
//                        polyline1.strokeColor = UIColor.red
//                        polyline1.strokeWidth = 3.0
//                        polyline1.map = self.mapView
//                        
//                        let position = CLLocationCoordinate2D(latitude: Double( self.current_latitude)!, longitude: Double(self.current_longitude)!)
//                        let marker = GMSMarker(position: position)
//                        marker.title = ""
//                        marker.map = self.mapView
//                        marker.icon = UIImage(named:"driver-car")

                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        })
        
        task.resume()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
