//
//  LoginViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/13/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var txtf_email: UITextField!

    @IBOutlet weak var txtf_pass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO:- set tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- remove Keyboard action of tapgesture
    func removeKeyboard(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    
    
     @IBAction func btn_backtoHomeAction(_ sender: Any)
     {
       _ = self.navigationController?.popViewController(animated: false)
     }
    
    
    
    
    
    // MARK:- btn_login Action
    @IBAction func btn_loginAction(_ sender: Any) {
        
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
//        self.navigationController?.pushViewController(vc, animated: false)
        
        
        
        
    
        if ((txtf_email.text?.characters.count)! > 0) && ((txtf_pass.text?.characters.count)! > 0)
        {
            if  isValidEmail(testStr: txtf_email.text!)  == true
            {
                if Reachability.isConnectedToNetwork() == true
                {
                    print("Internet Connection Available!")
                     self.callLoginMethod()
                }
                else
                {
                    print("Internet Connection not Available!")
                    DispatchQueue.main.async {
                        self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Please check your network connection")
                    }
                }
               
            }
            else
            {
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Email is not in correct format")
                }
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Field can't be empty")
                
                
            }
            
        }
    }
    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:-  server call of login method
    func callLoginMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "email": txtf_email.text!,
            "password": txtf_pass.text!,
            "device": "ios",
            "fcm_key": UserDefaults.standard.object(forKey:
            "fcm_token")!
        ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLoginUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
           // let status = result["status"] as? String
          //  if status != ""
           // {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
             
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    UserDefaults.standard.setValue(data["FirstName"], forKey: "name")
                
                    UserDefaults.standard.setValue(data["EmailAddress"], forKey: "email")
                    UserDefaults.standard.setValue(data["Mobile"], forKey: "mobile")
                    UserDefaults.standard.setValue(data["PassengerID"], forKey: "passengerID")
                    
                    UserDefaults.standard.setValue("", forKey: "profile_image")
                    UserDefaults.standard.synchronize()
                                      
                    // call next view controller in main queue
                    DispatchQueue.main.async {
                        
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            
        
        }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
