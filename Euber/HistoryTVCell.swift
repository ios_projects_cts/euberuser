//
//  HistoryTVCell.swift
//  Euber
//
//  Created by chawtech solutions on 2/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class HistoryTVCell: UITableViewCell {

     @IBOutlet weak var lbl_status: UILabel!
     @IBOutlet weak var lbl_dropLocation: UILabel!
     @IBOutlet weak var lbl_Name: UILabel!
     @IBOutlet weak var lbl_bookingTime: UILabel!
    @IBOutlet weak var view_historyBack: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.view_historyBack.backgroundColor = UIColor.white
        self.view_historyBack.layer.cornerRadius = 5.0
        // self.view_taxiBack.layer.borderWidth = 2.0
        self.view_historyBack.layer.borderColor = UIColor.clear.cgColor
        self.view_historyBack.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_historyBack.layer.shadowOpacity = 1.0
        self.view_historyBack.layer.shadowRadius = 1.0
        self.view_historyBack.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
