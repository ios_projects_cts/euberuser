//
//  BottomCVCell.swift
//  Euber
//
//  Created by chawtech solutions on 2/14/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class BottomCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var imageview: UIImageView!
}
