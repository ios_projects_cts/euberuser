//
//  ProfileViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/18/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
class ProfileViewController: UIViewController , UITableViewDelegate,UITableViewDataSource ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    
    //MARK:- declare var,objects and outlets
    @IBOutlet weak var tbl_placeslist: UITableView!
    
    @IBOutlet weak var txtf_mobile: UITextField!
    @IBOutlet weak var txtf_email: UITextField!
    @IBOutlet weak var txtf_lname: UITextField!
    @IBOutlet weak var txtf_fname: UITextField!
    @IBOutlet weak var txtf_password: UITextField!
     @IBOutlet weak var imgv_profilePic: UIImageView!
      var chosenImage = UIImage()
var activityIndicatorView: ActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Profile"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backItem?.title = ""
         activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        // crete navigation br button
        let btn_submit = UIButton(type: .custom)
        btn_submit.setTitle("Submit", for: .normal)
        btn_submit.setTitleColor(UIColor.white, for: .normal)
        btn_submit.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btn_submit.addTarget(self, action: #selector(ProfileViewController.submitbuttonAction), for:.touchUpInside)
        let submit_btn = UIBarButtonItem(customView: btn_submit)
        self.navigationItem.setRightBarButtonItems([submit_btn], animated: true)
        
        imgv_profilePic.layer.cornerRadius = imgv_profilePic.frame.size.height/2
        imgv_profilePic.clipsToBounds = true
        
        self.callgetProfileMethod()
    }
    //MARK:-  server call of login method
    func callgetProfileMethod()
    {
   //  self.showActivityIndicator(viewController: self,status: true)
        DispatchQueue.main.async {
        self.activityIndicatorView.startAnimating()
       // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let apiCall = JsonApi()
        let parameters = [
            
            
            "PassengerID": UserDefaults.standard.object(forKey:
                "passengerID")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalugetProfileUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                // set all key values in user defaults
                let data = result["data"] as! NSDictionary
                UserDefaults.standard.setValue(data["FirstName"], forKey: "firstName")
                UserDefaults.standard.setValue(data["LastName"], forKey: "lastName")
                UserDefaults.standard.setValue(data["photo"], forKey: "profile_image")
                UserDefaults.standard.setValue(data["EmailAddress"], forKey: "email")
                UserDefaults.standard.setValue(data["Mobile"], forKey: "mobile")
                UserDefaults.standard.setValue(data["PassengerID"], forKey: "passengerID")
                UserDefaults.standard.synchronize()
                
                
                
                //do something here that will taking time
                
              
                DispatchQueue.main.async {
                    
             //self.showActivityIndicator(viewController: self,status: false)
                 
                    self.activityIndicatorView.stopAnimating()
                self.txtf_fname.text = UserDefaults.standard.object(forKey: "firstName") as? String
               self.txtf_lname.text =  UserDefaults.standard.object(forKey: "lastName") as? String
                
                self.txtf_email.text = UserDefaults.standard.object(forKey: "email") as? String
                self.txtf_mobile.text = UserDefaults.standard.object(forKey: "mobile") as? String
              let profileImageUrl =  UserDefaults.standard.object(forKey: "profile_image") as? String
                    
                    if profileImageUrl != ""
                    {
                let imageData: Data = try! Data(contentsOf: URL(string: profileImageUrl!)!)
                self.imgv_profilePic.image = UIImage(data: imageData)!
                    }
                    else
                    {
                       // self.imgv_profilePic.image = UIImage(named: "profile-placeholder")
                    }
                }
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }

    //MARK:- menu button Action
    func submitbuttonAction()
    {
        if ((txtf_email.text?.characters.count)! > 0)
            && ((txtf_fname.text?.characters.count)! > 0)
            && ((txtf_lname.text?.characters.count)! > 0)
            && ((txtf_mobile.text?.characters.count)! > 0)
        {
            if  isValidEmail(testStr:txtf_email.text!)  == true
            {
                activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
                self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
                    // call register method
                    self.callupdateProfileMethods()
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Email is not in correct format")
                }
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Field can't be empty")
            }
            
        }
        

    }
//MARK:- check email format function
func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}
    //MARK:- access camera code
    func accessCamerafromdevice()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    //MARK:- access photos code
    func accessPhotosfromdevice()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    //MARK:- btn upload Vehicle Image action
    @IBAction func btn_uploadImage(_ sender: Any)
    {
        let alert = UIAlertController(title:"Pick Any One", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { action in
            // perhaps use action.title here
            
            self.accessCamerafromdevice()
        })
        alert.addAction(UIAlertAction(title: "Photos", style: .default) { action in
            // perhaps use action.title here
            
            self.accessPhotosfromdevice()
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            // perhaps use action.title here
        })
        
        self.present(alert, animated: true)
    }
    
    
    
    //MARK:- imagePickerController delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //  myImageView.contentMode = .scaleAspectFit //3
        // myImageView.image = chosenImage //4
       // btn_vehicle_image.setBackgroundImage(chosenImage, for: .normal)
        imgv_profilePic.image = chosenImage
        dismiss(animated:true, completion: nil) //5
    }
    

    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    func callupdateProfileMethods()
    {
        
        // call alert controller in main queue
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
        }
        let postPictureUrl = NSURL(string: kbaseUrl + kadditionalupdateProfileUrl)
        let request = NSMutableURLRequest(url: postPictureUrl! as URL)
        request.httpMethod="POST"
        
        var jsonString = String()
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        
        jsonObject.setValue(UserDefaults.standard.object(forKey:"passengerID"), forKey:"PassengerID")
        jsonObject.setValue(txtf_email.text!, forKey: "email")
        jsonObject.setValue(txtf_fname.text!, forKey: "FirstName")
        jsonObject.setValue(txtf_lname.text!, forKey: "LastName")
        jsonObject.setValue(txtf_mobile.text!, forKey: "phone")
        jsonObject.setValue(txtf_password.text!, forKey: "password")
        
        let jsonData: NSData
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json string = \(jsonString)")
            
            
        } catch _ {
            print ("JSON Failure")
        }
        
        
        let param = [
            
            "data": jsonString
        ]
        
        //  let abc =  KeychainWrapper.stringForKey("tokenValue")!
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var imageData = UIImageJPEGRepresentation(chosenImage, 0.1)
        
        if imageData==nil {
            imageData = NSData() as Data
            print("image data is nil")
        }
        
        request.httpBody = createBodyWithParameters(parameters:param, filePathKey: "photo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            // Print out reponse body
            // let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            // print("**** response data = \(responseString!)")
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: AnyObject] {
                    print(json)
                    let status = json["success"] as! String
                    let message = json["message"]as! String
                    
                    if status == "true"
                    {
                        // call alert controller in main queue
                        DispatchQueue.main.async {
                            self.activityIndicatorView.stopAnimating()
                            let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                               _ = self.navigationController?.popViewController(animated: false)
                            })
                            self.present(alert, animated: true)
                            
                        }
                        
                    }
                    else
                    {
                        // call alert controller in main queue
                        DispatchQueue.main.async {
                             self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title:"Message!" , message:message)
                        }
                    }
                    
                    
                }
            } catch let err {
                print(err)
            }
        }
        task.resume()
    }
    
    //    func generateBoundaryString() -> String {
    //        return "Boundary-\(NSUUID().uuidString)"
    //    }
    
    
    func createBodyWithParameters(parameters:[String:Any]?, filePathKey: String?, imageDataKey:NSData, boundary: String) -> NSData {
        
        let body=NSMutableData()
        
        if parameters != nil {
            for(key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }

    @IBAction func btn_favoritesAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! ProfileTVCell
//        let dict = historyListArr[indexPath.row] as? Dictionary<String,String>
//        cell.lbl_status.text = dict?["status"]
//        cell.lbl_dropLocation.text = dict?["drop_location"]
//        cell.lbl_Name.text = dict?["Name"]
//        cell.lbl_bookingTime.text = dict?["BookedTime"]
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
