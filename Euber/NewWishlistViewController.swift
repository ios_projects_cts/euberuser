//
//  NewWishlistViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/22/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GooglePlaces



// MARK:- Autocomplete delegate methods of google place api
extension NewWishlistViewController: GMSAutocompleteViewControllerDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // TODO: Handle the user's selection.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
        
        txtv_locName.text = place.name + place.formattedAddress!
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        // TODO:User canceled the operation.
//        if lbl_pickupAdd.text == "" {
//            lbl_pickupAdd.text = self.userAddress
//        }
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class NewWishlistViewController: UIViewController,UITextViewDelegate {

      var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var txtf_locTitle: UITextField!
    @IBOutlet weak var txtv_locName: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }

    //MARK:- btn cancel action
    @IBAction func btn_cancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    // MARK:- btn_add Favourites Action
    @IBAction func btn_addFavouritesAction(_ sender: Any)
    {
        if ((txtf_locTitle.text?.characters.count)! > 0) && ((txtv_locName.text?.characters.count)! > 0)
        {
            activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
            self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
            self.callAddFavouriteLocationMethod()
        }
        
    }

    func callAddFavouriteLocationMethod()
    {
        
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": UserDefaults.standard.object(forKey: "passengerID")!,
            "address": txtv_locName.text!,
            "locationtype": txtf_locTitle.text!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionaladdFavouritesUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let status = result["status"] as! String
            if status == "success"
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    DispatchQueue.main.async
                    {
                        self.activityIndicatorView.startAnimating()
                        self.dismiss(animated: true, completion: nil)
                        // UIApplication.shared.beginIgnoringInteractionEvents()
                    }
                    
                    
                }
                else
                {
                    // display alert controller
                    self.displayAlertWithMessage(viewController:self, title: message, message:"")
                }
            }
            else
            {
                
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
