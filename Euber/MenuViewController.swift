//
//  MenuViewController.swift
//  YouTowVender
//
//  Created by chawtech solutions on 1/18/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- declaration of var,objects and outlets
    var tableView: UITableView  =   UITableView()
    @IBOutlet var profileName: UILabel!
     @IBOutlet var profile_pic: UIImageView!
    
    
    var menuArr = ["Home","Payment","History","Notifications","Wallet","Settings","Help","Logout"]
   var menuImageArr = ["home","payment","history","notification","wallet","settings","help","logout"]
    
    
    
    //MARK:- viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //TODO:- create tableview programatically
        tableView = UITableView(frame: (CGRect (x:0, y:100 , width: self.view.frame.size.width, height: self.view.frame.size.height)), style: UITableViewStyle.plain)
        tableView.delegate      =   self
        tableView.dataSource    =   self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.view.addSubview(tableView)
        //tableView.register(UINib(nibName: "VehicleTypeTVCell", bundle: nil), forCellReuseIdentifier: "cell")
        // tableView.layer.borderColor = UIColor.lightGray.cgColor
        // tableView.layer.borderWidth = 0.5
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        
        profileName.text = UserDefaults.standard.object(forKey:"name") as? String
        profile_pic.layer.cornerRadius = profile_pic.frame.size.height/2
        profile_pic.clipsToBounds = true
        let profileImageUrl =  UserDefaults.standard.object(forKey:"profile_image") as? String
        if (profileImageUrl != "") //&& (profileImageUrl != nil)
        {
            let imageData: Data = try! Data(contentsOf: URL(string: profileImageUrl!)!)
            self.profile_pic.image = UIImage(data: imageData)!
        }
        else
        {
            self.profile_pic.image = UIImage (named: "profile-placeholder")
        }

    }
    
    //MARK:- tableview delagates method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)
        cell.textLabel?.text = menuArr[indexPath.row]
        cell.imageView?.image = UIImage(named:menuImageArr[indexPath.row])
        
        let itemSize:CGSize = CGSize(width:20,height: 20)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        let imageRect : CGRect = CGRect(x:0, y:0,width: itemSize.width, height:itemSize.height)
        cell.imageView!.image?.draw(in: imageRect)
        cell.imageView!.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        cell.textLabel?.font = UIFont .systemFont(ofSize: 15)
        cell.textLabel?.textColor = UIColor.black
        cell.selectionStyle = .gray
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // TODO:- handle clicks of side menu
        let centerSubChild = NotificationCenter.default
    
        if indexPath.row == 0
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "home"]
            
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }
        else if indexPath.row == 1
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "payment"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }
        else if indexPath.row == 2
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "history"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
       }
        if indexPath.row == 3
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "notifications"]
            
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }
        else if indexPath.row == 4
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "wallet"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }

        else if indexPath.row == 5
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "settings"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }
        else if indexPath.row == 6
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "help"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }
        else if indexPath.row == 7
        {
            let userInfo: [AnyHashable: Any] = [
                "name" : "logout"]
            centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        }

    }
    @IBAction func btn_profileAction(_ sender: Any)
    {
        //                let storyboard = UIStoryboard(name:"Main", bundle: nil)
        //                let vc  = storyboard.instantiateViewController(withIdentifier:"ProfileViewController") as! ProfileViewController
        //                self.navigationController?.pushViewController(vc, animated: true)
        //        self.revealViewController().revealToggle(self)
        
        
        let centerSubChild = NotificationCenter.default
        
        
        let userInfo: [AnyHashable: Any] = [
            "name" : "profile"]
        
        centerSubChild.post(name: NSNotification.Name(rawValue: "gotosubchild"), object: self, userInfo: userInfo)
        
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
