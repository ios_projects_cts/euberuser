//
//  WishListTVCell.swift
//  Euber
//
//  Created by chawtech solutions on 2/15/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class WishListTVCell: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
     @IBOutlet weak var view_bck: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.view_bck.backgroundColor = UIColor.white
        self.view_bck.layer.cornerRadius = 5.0
        // self.view_taxiBack.layer.borderWidth = 2.0
        self.view_bck.layer.borderColor = UIColor.clear.cgColor
        self.view_bck.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_bck.layer.shadowOpacity = 1.0
        self.view_bck.layer.shadowRadius = 1.0
        self.view_bck.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
