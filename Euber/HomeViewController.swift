
//
//  HomeViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/13/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//


import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import MapKit

// MARK:- Autocomplete delegate methods of google place api
extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // TODO: Handle the user's selection.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
        
        lbl_pickupAdd.text = place.name + place.formattedAddress!
        self.getlatLongFromAddress(address: lbl_pickupAdd.text!)
      // destinationAddress = place.name + place.formattedAddress!
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        // TODO:User canceled the operation.
         lbl_pickupAdd.text = ""
        self.determineCurrentLocation()
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class HomeViewController: UIViewController,CLLocationManagerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UIGestureRecognizerDelegate,GMSMapViewDelegate {
    
    //MARK:- declaration var objects and outlets
    @IBOutlet weak var view_topCollectionviewBack: UIView!
    @IBOutlet weak var view_locationpickTop: UIView!
    @IBOutlet weak var coll_bottom: UICollectionView!
    @IBOutlet weak var coll_top: UICollectionView!
    @IBOutlet weak var lbl_pickupAdd: UILabel!
    @IBOutlet weak var btn_searchLocationPin: UIButton!
    var locationManager:CLLocationManager!
    var userAddress = String()
    var destinationAddress = String()
    var current_latitude = String()
    var current_longitude = String()
    var placeMark = CLPlacemark()
    @IBOutlet weak var mapView : GMSMapView!
    var  DriverListArr = [[String: Any]]()
    var  tripClassListArr = NSMutableArray()
    var  driverFilterListArr = [[String: Any]]()
    var activityIndicatorView: ActivityIndicatorView!
    var tripClassId = String()
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
     var markerImg = UIImageView()
     let marker = GMSMarker()
    
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        view_topCollectionviewBack.isHidden = true
        coll_bottom.reloadData()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.view.backgroundColor = UIColor.white
       // self.navigationController?.navigationBar.barTintColor = UIColor.clear
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.fireMenuSubChild), name: NSNotification.Name(rawValue:"gotosubchild"), object: nil)
        
        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
    
        NotificationCenter.default.addObserver(
            self,selector: #selector(registerFcmTokentoServer),
            name:NSNotification.Name(rawValue: "REGISTERFCMTOKEN"),object: nil)

        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.favouriteproviderAddress(_:)), name: NSNotification.Name(rawValue: "favouriteprovider"), object: nil)
    }
    
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        // set notification center remove observber
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"gotosubchild"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"notificationFire"), object: nil)
        
        // Register to receive notification in your class
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "favouriteprovider"), object: nil)
        
        NotificationCenter.default.removeObserver(
            self,
            name:NSNotification.Name(rawValue:"REGISTERFCMTOKEN"),object: nil)
    }
    
    func registerFcmTokentoServer(notification: NSNotification)
    {
        if let userInfo = notification.object as? [String:AnyObject] {
            let refreshToken = userInfo["refreshtoken"] as? String
            if refreshToken != nil {
                callupdateFcmTokenToServerMethod(refreshFcmToken:refreshToken!)
            }
            
        }
    }

    //MARK:-  server call of callupdateFcmTokenToServer Method
    func callupdateFcmTokenToServerMethod(refreshFcmToken:String)
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": UserDefaults.standard.object(forKey:
                "passengerID")!,
            "fcm_key": refreshFcmToken,
           
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalupdateFcmTokensUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
        
    }
    

    // handle notification
    func favouriteproviderAddress(_ notification: NSNotification) {
        
        let address = notification.userInfo?["address"] as? String
        
        lbl_pickupAdd.text = address
    }
    
    
    
    // handle notification
    func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"] as! [String:AnyObject]
        print(data)
        
       let notification =  data["notificaton_type"]as? String
       let notification_type = notification?.replacingOccurrences(of:"\"", with:"")
        
               // let notification_type = String(describing:data["notificaton_type"])
                if notification_type! == "Ride Completed."
                {
        
                }
                if notification_type! == "Booking Confirmed."
                {

        // create the alert
        let alert = UIAlertController(title:"Message", message:"Bokking Confirmed", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
                vc.userInfo = data 
               // self.navigationController?.pushViewController(vc, animated: false)
                self.present(vc, animated: true, completion: nil)
            }
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        }
        
//         DispatchQueue.main.async {
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
//       // vc.userInfo = data as! [String : String] as [String : AnyObject]
//        self.navigationController?.pushViewController(vc, animated: false)
//        
//        
//        // call alert controller in main queue
//       // DispatchQueue.main.async {
//            self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Bokking Confirmed")
//            
//          
//        }

    }
    
//    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
//        //if (marker.userData? as AnyObject).isEqualtoString("xMark") {
//            print("marker dragged to location: \(marker.position.latitude),\(marker.position.longitude)")
//       // }
//    }
    
    
    
    // MARK:- get cordinate tap on google map
     // func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        OperationQueue.main.addOperation {() -> Void in
//            
//            print(coordinate.latitude)
//            print(coordinate.longitude)
////                        if myMarker {
////                            myMarker.map = nil
////                            myMarker = nil
////                        }
//                       let myMarker = GMSMarker()
//                        myMarker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
//                        myMarker.title = "title"
//                        myMarker.icon = UIImage(named:"driver-car")
//                        myMarker.map = mapView
//        }
//    }
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        //MARK:- set search loaction pin
         markerImg = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(48), height: CGFloat(48)))
        markerImg.center = CGPoint(x: CGFloat(self.view.center.x), y: CGFloat(self.view.center.y - 24))
        markerImg.image = UIImage(named:"searchpin")
        view_locationpickTop.backgroundColor = UIColor.clear
        mapView.alpha = 0.85
        mapView.delegate = self
        self.view.addSubview(markerImg)
        
        
        let notifBack = appDelegate.notifBack
        if notifBack == true
        {
            let data = appDelegate.UserInfo
            
            //let data = notification.userInfo?["data"]
            print(data)
            
            // create the alert
            
            let notification =  data["notificaton_type"]as? String
            let notification_type = notification?.replacingOccurrences(of:"\"", with:"")
            
            // let notification_type = String(describing:data["notificaton_type"])
            if notification_type! == "Ride Completed."
            {
                
            }
            if notification_type! == "Booking Confirmed."
            {

            let alert = UIAlertController(title:"Message", message:"Bokking Confirmed", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"MapRouteViewController") as! MapRouteViewController
                    vc.userInfo = data as! [String : AnyObject]
                    // self.navigationController?.pushViewController(vc, animated: false)
                    self.present(vc, animated: true, completion: nil)
                }
                
            }))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
        }

        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        
        
//        //MARK:- set effect of uiview for search
//        
//        self.view_searchBack.backgroundColor = UIColor.white
//        self.view_searchBack.layer.cornerRadius = 3.0
//        self.view_searchBack.layer.borderWidth = 2.0
//        self.view_searchBack.layer.borderColor = UIColor.clear.cgColor
//        self.view_searchBack.layer.shadowColor = UIColor.lightGray.cgColor
//        self.view_searchBack.layer.shadowOpacity = 1.0
//        self.view_searchBack.layer.shadowRadius = 1.0
//        self.view_searchBack.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(3))
        
        
        self.navigationController?.navigationBar.isHidden = true
        view_topCollectionviewBack.backgroundColor = UIColor.clear
        coll_top.backgroundColor = UIColor.clear
         view_topCollectionviewBack.isHidden = true
        
        self.determineCurrentLocation()
        self.calltripClassesMethod()
        
        // TODO:- set tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeTopCollectionview(_:)))
        tapGesture.delegate = self
        self.view_topCollectionviewBack.addGestureRecognizer(tapGesture)
        
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                 self.determineCurrentLocation()
            }
        } else {
            print("Location services are not enabled")
            self.displayAlertWithMessage(viewController: self, title: "Message", message: "Location services are not enabled \n please check your settings")
        }
        
      
        
        // TODO:- set tap gesture recognizer
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard(_:)))
        self.mapView.addGestureRecognizer(tap)
        
        
      

    }
    
    
    //MARK:- remove Keyboard action of tapgesture
    func removeKeyboard(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }


    //MARK:- callPlacePicker action of tapgesture
    func removeTopCollectionview(_ sender: UITapGestureRecognizer) {
        
        
      view_topCollectionviewBack.isHidden = true
        coll_bottom.reloadData()
        
    }
    @IBAction func btn_pickUpAction(_ sender: Any)
    {
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    
    // MARK:- btn_logcation Action
    @IBAction func btn_serachLocationAction(_ sender: Any)
    {
       
//        if (btn_searchLocationPin.isSelected == true)
//        {
//             mapView.delegate = self
//            self.view.addSubview(markerImg)
//            btn_searchLocationPin.isSelected = false;
//        }
//        else
//        {
//            lbl_pickupAdd .text = ""
//            mapView.delegate = nil
//            markerImg.removeFromSuperview()
//            btn_searchLocationPin.isSelected = true
//            if lbl_pickupAdd.text == ""
//            
//            {
//                lbl_pickupAdd.text = self.userAddress
//            }
//        }
    }
    
    
    // MARK:- btn_logcation Action
    @IBAction func btn_locationAction(_ sender: Any)
    {
        self.determineCurrentLocation()
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let lat: Double = position.target.latitude
        let lng: Double = position.target.longitude
        print(lat)
        print(lng)
        
//        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
//        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
//        
//        DispatchQueue.main.async {
//            self.activityIndicatorView.startAnimating()
//            // UIApplication.shared.beginIgnoringInteractionEvents()
//        }
        
        // get formatted address of current user from geolocation
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat, longitude: lng)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
             //    print(addressDict)
            //Print each key-value pair in a new row
           //  addressDict.forEach { print($0) }
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
               // DispatchQueue.main.async {
                    //self.activityIndicatorView.startAnimating()
                     self.lbl_pickupAdd.text = formattedAddress.joined(separator: ", ")
                   
                //}
               
                
            }

        });
        
    }
    
    func getlatLongFromAddress(address:String)
        {
            activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
            self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
            
            DispatchQueue.main.async {
                self.activityIndicatorView.startAnimating()
                
            }

       
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
        if((error) != nil){
        //print("Error", error)
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                self.displayAlertWithMessage(viewController: self, title: "Message", message: "Location not Found in Map")
            }
        }
       
        if let placemark = placemarks?.first {
        let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
//        coordinates.latitude
//        coordinates.longitude
        print("lat", coordinates.latitude)
        print("long", coordinates.longitude)
        
            let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude,
            longitude:coordinates.longitude, zoom: 12)
            // mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-40), camera: camera)
            self.mapView.camera = camera
            self.mapView.isMyLocationEnabled = false

          
                        self.marker.position = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
                        self.marker.title = address
                       // marker.snippet = self.userAddress
                       // marker.isDraggable = true
                        self.marker.map = self.mapView
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                
            }
        }
        });
    }
//    func getLocationFromAddressString(_ addressStr: String) -> CLLocationCoordinate2D {
//        let latitude: Double = 0
//        let longitude: Double = 0
//        var esc_addr = addressStr.addingPercentEscapes(using: String.Encoding.utf8)
//        var req: String = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=\(esc_addr)"
//        var result = try? String(contentsOf: URL(string: req)!, encoding: String.Encoding.utf8)
//        if result != nil {
//            var scanner = Scanner(string: result!)
//            if scanner.scanUpTo("\"lat\" :", into: nil) && scanner.scanString("\"lat\" :", into: nil) {
//                scanner.scanDouble(latitude)
//                if scanner.scanUpTo("\"lng\" :", into: nil) && scanner.scanString("\"lng\" :", into: nil) {
//                    scanner.scanDouble(longitude)
//                }
//            }
//        }
//        var center: CLLocationCoordinate2D
//    }
    
  /*  func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        var lat: Double = position.target.latitude
        var lng: Double = position.target.longitude
        var location = CLLocation(latitude: lat, longitude: lng)
        //var location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
        print(location)
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
                print(pm)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })    }
    
   */
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if gesture {
            //self.lbl_pickupAdd.text = "Getting Address..."
        }
    }
    
    func mapView(_ pMapView: GMSMapView, didChange position: GMSCameraPosition) {
    }
    
    @IBAction func btn_menuAction(_ sender: Any)
    {
        
//        let mainVC = MenuViewController(nibName:"MenuViewController", bundle:nil)
//        mainVC.view.frame = CGRect(x:0,y:0, width:150, height:568)
//    //    self.navigationController.pushViewController(mainVC, true);
//        let animation = CATransition()
//        animation.duration = 0.5
//        animation.type = kCATransitionPush
//        animation.subtype = kCATransitionFromRight
//        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        mainVC.view.layer.add(animation, forKey: "SwitchToView")
//        
//        self.present(mainVC, animated: true, completion: nil)
        
        
        
        let reveal: SWRevealViewController? = self.revealViewController()
        reveal?.revealToggle(self)
        
        
      //  self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    
        
        
//        let controller:ForgetViewController = self.storyboard!.instantiateViewController(withIdentifier: "ForgetViewController") as! ForgetViewController
//      //  controller.ANYPROPERTY=THEVALUE // If you want to pass value
//        controller.view.frame = CGRect(x:0,y:0, width:150, height:568)
//        controller.willMove(toParentViewController: self)
//        self.view.addSubview(controller.view)
//        self.addChildViewController(controller)
//        //controller.didMove(toParentViewController: self)
    }
   
    
    // MARK:- handle side menu button notification
    func fireMenuSubChild(_ notification: NSNotification) {
        
        if notification.userInfo?["name"]as! String == "home"
        {
               self.revealViewController().revealToggle(self)
        }
        if notification.userInfo?["name"]as! String == "payment"
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is PaymentViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"PaymentViewController") as! PaymentViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
                
            }
        }
        else  if notification.userInfo?["name"]as! String == "history"
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is HistoryViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"HistoryViewController") as! HistoryViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
                
            }
            
        }
        if notification.userInfo?["name"]as! String == "notifications"
            
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is NotificationsViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"NotificationsViewController") as! NotificationsViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
            
        }
        
        if notification.userInfo?["name"]as! String == "wallet"
            
        {
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is WalletViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"WalletViewController") as! WalletViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
            
        }
        if notification.userInfo?["name"]as! String == "settings"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is SettingsViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"SettingsViewController") as! SettingsViewController
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            

        }
        if notification.userInfo?["name"]as! String == "help"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is HelpViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"HelpViewController")
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            

        }
        
        if notification.userInfo?["name"]as! String == "profile"
            
        {
            
            var viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is ProfileViewController) {
                    viewControllerAlreadyPushed = true
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ProfileViewController")
                self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)            }
            
            
        }

        if notification.userInfo?["name"]as! String == "logout"
            
        {
            DispatchQueue.main.async {
                self.activityIndicatorView.startAnimating()
                // UIApplication.shared.beginIgnoringInteractionEvents()
            }

    let apiCall = JsonApi()
    let parameters = [
        
        "PassengerID": UserDefaults.standard.object(forKey: "passengerID")!
        
    ]
    
    apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLogoutUrl, para: parameters as (AnyObject), isSuccess: true)
    { (result) -> Void in
    
    print("service-",result)
       
    let status = result["status"] as! String
    if status == "success"
    {
    let success = result["success"] as! String
    let message = result["message"] as! String
    
    print(status)
    if success == "true"
    {
    self.callLogoutSuccessMethod()
    }
    else
    {
    // display alert controller
    self.displayAlertWithMessage(viewController:self, title: message, message:"")
    }
    }
    else
    {
    
    }
    
        }
        }
    }
    
    
    func callLogoutSuccessMethod() {
        // remove objects from key value in user defaults
//        if let bundle = Bundle.main.bundleIdentifier {
//            UserDefaults.standard.removePersistentDomain(forName: bundle)
//        }
        
        UserDefaults.standard.removeObject(forKey:"name")
        UserDefaults.standard.removeObject(forKey:"email")
        UserDefaults.standard.removeObject(forKey:"mobile")
        UserDefaults.standard.removeObject(forKey:"passengerID")
       
        
        // call next view controller in main queue
        DispatchQueue.main.async {
            
            self.activityIndicatorView.stopAnimating()
            let viewControllerAlreadyPushed: Bool = false
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is LoginViewController) {
                    
                    controller .removeFromParentViewController()
                    // viewControllerAlreadyPushed = true
                    
                }
            }
            if !viewControllerAlreadyPushed {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                _ =  self.navigationController?.pushViewController(vc, animated: false)
                self.revealViewController().revealToggle(self)
            }
            
        }
        
    }

    
    //MARK:- determine Current Location for user
    func determineCurrentLocation()
    {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        
//                if CLLocationManager.locationServicesEnabled() {
//                    //locationManager.startUpdatingHeading()
//                    locationManager.startUpdatingLocation()
//                }
        
//        
//        let location = self.locationManager.location
//        
//        let latitude: Double = location!.coordinate.latitude
//        let longitude: Double = location!.coordinate.longitude
//        
//        print("current latitude :: \(latitude)")
//        print("current longitude :: \(longitude)")
    }
    
    
    //MARK:- location manager delagate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let userLocation:CLLocation = locations[0] as CLLocation
       // guard let userLocation = locations.first else { return }
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        locationManager.stopUpdatingLocation()
        
        current_latitude = String(userLocation.coordinate.latitude)
        current_longitude = String(userLocation.coordinate.longitude)
        
      //  let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude,
        longitude:userLocation.coordinate.longitude, zoom: 12)
        // mapView = GMSMapView.map(withFrame:CGRect(x:0,y:0,width:self.view.frame.size.width,height:self.view.frame.size.height-40), camera: camera)
        mapView.camera = camera
        mapView.isMyLocationEnabled = false
        //  self.view.addSubview(mapView)
       
        self.callGetDriverLocationMethod()
        
        // get formatted address of current user from geolocation
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            
            
          //  print(addressDict)
             //Print each key-value pair in a new row
           //    addressDict.forEach { print($0) }
            
            
            // Print fully formatted address
            if let formattedAddress = addressDict["FormattedAddressLines"] as? [String] {
                print(formattedAddress.joined(separator: ", "))
                self.userAddress = formattedAddress.joined(separator: ", ")
                self.lbl_pickupAdd.text = formattedAddress.joined(separator: ", ")
                
            }
                 /*       // Access each element manually
                        if let locationName = addressDict["Name"] as? String {
                            print(locationName)
                        }
                        // Access each element manually
                        if let locationstreet = addressDict["Street"] as? String {
                            print(locationstreet)
                        }
                        if let street = addressDict["Thoroughfare"] as? String {
                            print(street)
                        }
                        if let city = addressDict["City"] as? String {
                            print(city)
                        }
                        if let zip = addressDict["ZIP"] as? String {
                            print(zip)
                        }
                        if let country = addressDict["Country"] as? String {
                            print(country)
                        }
            if let locality = addressDict["Locality"] as? String {
                print(locality)
            }
*/
//            
//                                    if myMarker {
//                                        myMarker.map = nil
//                                        myMarker = nil
//                                    }

           
//            self.marker.position = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
//          //  self.marker.title = (addressDict["Name"] as? String)!
//            self.marker.snippet = self.userAddress
//            self.marker.isDraggable = true
//            self.marker.map = self.mapView
            
        })
        
        
//        var geocoder = CLGeocoder()
//        var location1 = CLLocation(latitude: 28.618324, longitude: 77.372595)
//        geocoder.reverseGeocodeLocation(location1) {
//            (placemarks, error) -> Void in
//            if let placemark1 = placemarks , (placemarks?.count)! > 0 {
//                let placemark = placemark1[0]
//                print(placemark.addressDictionary)
//            }
//        
        }
 
    
      /*  //--- CLGeocode to get address of current location ---//
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil)
            {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0
            {
                let pm = (placemarks?[0])! as CLPlacemark
                self.displayLocationInfo(placemark: pm)
            }
            else
            {
                print("Problem with the data received from geocoder")
            }
        })
        
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark?)
    {
        if let containsPlacemark = placemark
        {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let country = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            
            print(placemark)
            print(locality)
            print(postalCode)
            print(administrativeArea)
            print(country)
        }
        
    }
    
    */
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- gesture recognizer delegate method
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        
        if touch.view == self.view_topCollectionviewBack {
            
            return true
        }
        return false
        
    }
    
    
    //MARK:- server call of get vednder location method
    func callGetDriverLocationMethod()
    {
        
        
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "TripClassTypesID": "",
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetVenderUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // parse data from server
                    let venderArr=result["data"] as? [[String: Any]]
                    self.parseGetLocationListData(dataArray: venderArr!)
                }
                else
                {
                    DispatchQueue.main.async
                        {
                            self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                    
                }
            }
            else
            {
                
            }
        }
        
    }
    
    //  MARK:- parseGetLocationListData
    func parseGetLocationListData(dataArray:[[String: Any]])
    {
        DriverListArr.removeAll()
        let venderDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            venderDict.setValue((dataArray[i]["DriverID"] as? NSString), forKey: "DriverID")
            venderDict.setValue((dataArray[i]["distance"] as? NSString), forKey: "distance")
            venderDict.setValue((dataArray[i]["LocGPSLatitude"] as? NSString), forKey: "LocGPSLatitude")
            venderDict.setValue((dataArray[i]["LocGPSLongitude"] as? NSString), forKey: "LocGPSLongitude")
            venderDict.setValue((dataArray[i]["FirstName"] as? NSString), forKey: "name")
            
            DriverListArr.append(venderDict .mutableCopy() as! [String : Any])
            
            
            
        }
        
        for i in 0...DriverListArr.count-1
        {
            let lat =  (DriverListArr[i]["LocGPSLatitude"] as? NSString)?.doubleValue
            let long = (DriverListArr[i]["LocGPSLongitude"] as? NSString)?.doubleValue
            
            let latitude =  NSNumber(value:lat!)
            let longitude =  NSNumber(value:long!)
            
            
            DispatchQueue.main.async {
                
                let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
                let marker = GMSMarker(position: position)
                marker.title = (self.DriverListArr[i]["name"] as? String)
                marker.map = self.mapView
                marker.icon = UIImage(named:"driver-car")
            }
            
            
        }
        
    }

    //MARK:-  server call of login method
    func calltripClassesMethod()
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
           
           
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgettripclassUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
            
            // let status = result["status"] as? String
            //  if status != ""
            // {
            let success = result["success"] as! String
            
            
            if success == "true"
            {
                // parse data from server
                let venderArr=result["data"] as? [[String: Any]]
                self.parseGetTripClassListData(dataArray: venderArr!)
            }
            
        }
        
        
    }
    //  MARK:- parseGetLocationListData
    func parseGetTripClassListData(dataArray:[[String: Any]])
    {
        tripClassListArr.removeAllObjects()
        
        let tripDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            tripDict.setValue((dataArray[i]["ClassOrder"] as? NSString), forKey: "ClassOrder")
            tripDict.setValue((dataArray[i]["distance"] as? NSString), forKey: "distance")
            tripDict.setValue((dataArray[i]["Name"] as? NSString), forKey: "Name")
            tripDict.setValue((dataArray[i]["PriceFactor"] as? NSString), forKey: "PriceFactor")
            tripDict.setValue((dataArray[i]["TripClassTypesID"] as? NSString), forKey: "TripClassTypesID")
            tripClassListArr.add(tripDict.mutableCopy() )
            
            
        }
        
        DispatchQueue.main.async
            {
             self.coll_bottom.reloadData()
            self.activityIndicatorView.stopAnimating()
        
        }
    }
    
    //MARK:- server call of get vednder location method
    func callGetDriverLocationFilterMethod(tripClassid:String)
    {
        
//        self.driverFilterListArr.removeAllObjects()
//        self.coll_top.reloadData()
        
        
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "TripClassTypesID": tripClassid,
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetVenderUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                
                print(status)
                if success == "true"
                {
                    // parse data from server
                    let venderArr=result["data"] as? [[String: Any]]
                    self.parseGetLocationFilterListData(dataArray: venderArr!)
                }
                else
                {
                    DispatchQueue.main.async
                        {
                           self.activityIndicatorView.stopAnimating()
                            self.coll_bottom.reloadData()
                             // self.view_topCollectionviewBack.isHidden = true
                            self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                    
                }
            }
            else
            {
                
            }
        }
        
    }
    
    //  MARK:- parseGetLocationListData
    func parseGetLocationFilterListData(dataArray:[[String: Any]])
    {
       // driverFilterListArr.removeAllObjects()
        let venderDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            venderDict.setValue((dataArray[i]["DriverID"] as? NSString), forKey: "DriverID")
            venderDict.setValue((dataArray[i]["distance"] as? NSString), forKey: "distance")
            venderDict.setValue((dataArray[i]["LocGPSLatitude"] as? NSString), forKey: "LocGPSLatitude")
            venderDict.setValue((dataArray[i]["LocGPSLongitude"] as? NSString), forKey: "LocGPSLongitude")
            venderDict.setValue((dataArray[i]["FirstName"] as? NSString), forKey: "FirstName")
            
            driverFilterListArr.append(venderDict .mutableCopy() as! [String : Any])
            
            
            
        }
        
        for i in 0...driverFilterListArr.count-1
        {
            let lat =  (driverFilterListArr[i]["LocGPSLatitude"] as? NSString)?.doubleValue
            let long = (driverFilterListArr[i]["LocGPSLongitude"] as? NSString)?.doubleValue
            
            let latitude =  NSNumber(value:lat!)
            let longitude =  NSNumber(value:long!)
            
            
            DispatchQueue.main.async {
                
                 self.activityIndicatorView.stopAnimating()
                let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
                let marker = GMSMarker(position: position)
                marker.title = (self.driverFilterListArr[i]["FirstName"] as? String)
                marker.map = self.mapView
                marker.icon = UIImage(named:"driver-car")
            }
            
            
        }

//        DispatchQueue.main.sync
//            {
//               
//            coll_top.reloadData()
//            self.activityIndicatorView.stopAnimating()
//                        
//                
//               
//                
//        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == coll_bottom
        {
             return tripClassListArr.count
        }
        else
        {
            return driverFilterListArr.count
        }
       
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
          if collectionView == coll_bottom {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! BottomCVCell
        
        let dict = tripClassListArr[indexPath.row] as? Dictionary<String,String>
        cell.lbl_title.text = dict?["Name"]
          //  cell.lbl_subtitle.text = "12 mins"
        
            
            if cell.isSelected {
                cell.imageview.image = UIImage (named: "business-green")
                 cell.lbl_title.textColor = UIColor.init(red: 129/255.0, green: 180/255.0, blue: 65/255.0, alpha: 1)
            }
            else
            {
                cell.imageview.image = UIImage (named: "business")
                 cell.lbl_title.textColor = UIColor.black
            }
            
       // cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
        }
        else
          {
              // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! TopCVCell
            
            let dictdriver = driverFilterListArr [indexPath.row] as? Dictionary<String,String>
        
            cell.lbl_tripClassName.text = dictdriver?["FirstName"]
            cell.backgroundColor = UIColor.clear
            return cell
        }
    }
    
    
    // MARK: - UICollectionViewDelegate protocol

     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dict = tripClassListArr[indexPath.row] as? Dictionary<String,String>
        
          tripClassId = (dict?["TripClassTypesID"])!
        
        if collectionView == coll_bottom
        {
            //coll_bottom.selectItem(at: indexPath, animated: true, scrollPosition:.left)
        let cell: BottomCVCell? = collectionView.cellForItem(at: indexPath) as! BottomCVCell?
       
           // view_topCollectionviewBack.isHidden = false
            cell?.lbl_title.textColor = UIColor.init(red: 129/255.0, green: 180/255.0, blue: 65/255.0, alpha: 1)
            cell?.imageview.image = UIImage (named:"business-green")
           
            DispatchQueue.main.async
                {
                    self.activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
                    self.view.addSubview(self.activityIndicatorView.getViewActivityIndicator())
                    self.activityIndicatorView.startAnimating()
                    self.callGetDriverLocationFilterMethod(tripClassid:self.tripClassId)
            }

            
            
        
        }
        else
        {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"ConfirmBookingViewController") as! ConfirmBookingViewController
            vc.current_latitude = self.current_latitude
            vc.current_longitude = self.current_longitude
            vc.userAddress = self.userAddress
            vc.TripClassTypesID = tripClassId
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == coll_bottom
        {
            //coll_bottom.deselectItem(at: indexPath, animated: false)
        let cell: BottomCVCell? = collectionView.cellForItem(at: indexPath) as! BottomCVCell?
            cell?.imageview.image = UIImage (named:"business")
            cell?.lbl_title.textColor = UIColor.black
           

        }
    }
    
    @IBAction func btn_nextAction(_ sender: Any)
    {
        
        if (lbl_pickupAdd.text?.characters.count)! > 0 {
            
            if tripClassId != ""
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ConfirmBookingViewController") as! ConfirmBookingViewController
                vc.current_latitude = self.current_latitude
                vc.current_longitude = self.current_longitude
                vc.userAddress = userAddress
                vc.pickupaddAddress = lbl_pickupAdd.text!
                vc.TripClassTypesID = tripClassId
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                DispatchQueue.main.async {
                     self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "Please select trip class")
                }
               
            }
        }
        else
        {
             DispatchQueue.main.async {
            self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "Please Select Pickup Location")
            }
        }
        

    }
    // MARK:- btn_wishList Action
    @IBAction func btn_wishlistAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "WishListViewController") as! WishListViewController
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    //MARK:- btn_bookride Action
//    @IBAction func btn_bookrideAction(_ sender: Any) {
//        
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier: "BookQuoteViewController") as! BookQuoteViewController
//        self.navigationController?.pushViewController(vc, animated: false)
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
