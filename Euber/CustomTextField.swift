//
//  CustomTextField.swift
//  Euber
//
//  Created by chawtech solutions on 3/4/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
//    func textRectForBounds(bounds: CGRect) -> CGRect {
//        return super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(5, 0, -5, 0)))
//    }
//    
//    func editingRectForBounds(bounds: CGRect) -> CGRect {
//        return super.editingRect(forBounds: UIEdgeInsetsInsetRect(bounds,  UIEdgeInsetsMake(5, 0, -5, 0)))
//    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 8.0, dy: 8.0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
}
