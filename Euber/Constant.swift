//
//  Constant.swift
//  YouTow
//
//  Created by chawtech solutions on 1/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import SystemConfiguration


//MARK:- server url
var kbaseUrl = "http://35.167.21.236/euberAPI/"
var kadditionalLoginUrl = "passenger/login"
var kadditionalRegisterUrl = "passenger/registration"
var kadditionalgettripclassUrl = "passenger/get_trip_class"
var kadditionalgetVenderUrl = "passenger/getDriver"
var kadditionalbookCarUrl = "passenger/bookCar"
var kadditionalLogoutUrl = "passenger/logout"
var kadditionalgetGetHistoryUrl = "passenger/getHistory"
var kadditionalupdateProfileUrl = "passenger/editprofile"
var kadditionalugetProfileUrl = "passenger/getProfile"
var kadditionalgetFavouritesUrl = "passenger/getLocation"
var kadditionaladdFavouritesUrl = "passenger/addlocation"
var kadditionalgetPriceUrl = "passenger/getEstimate"
var kadditionalcancelBookingUrl = "passenger/cancelBooking"
var kadditionalgetBookingDetailsUrl = "passenger/getBookingDetails"
var kadditionalupdateFcmTokensUrl = "passenger/updateFCM"


public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

extension UIViewController {
    
    func backViewController(vc:UIViewController, animated:Bool)
    {
        self.navigationController?.pushViewController(vc,animated:animated)
    }
}


extension UIViewController {
    
    //MARK:- set alert controller globally
    func showActivityIndicator(viewController: UIViewController,status:Bool)
    {
        let activityView:UIView = UIView(frame: CGRect(x:0, y:0,width: self.view.frame.size.width, height:self.view.frame.size.height))
        activityView.backgroundColor = UIColor.lightText
       
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x:0.0,y: 0.0,width: 40.0, height:40.0)
        actInd.center = activityView.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.gray
        activityView.addSubview(actInd)
      
        if  status == true
        {
            
             self.view .addSubview(activityView)
             actInd.startAnimating()
        }
        else
        {
            activityView.removeFromSuperview()
            actInd.removeFromSuperview()
            actInd.stopAnimating()
           
        }
        
        //activityIndicator.startAnimating()
       
    }
    
}

extension UIViewController {
    
    //MARK:- set alert controller globally
    func displayAlertWithMessage(viewController: UIViewController, title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
        viewController.present(ac, animated: true)
    }
}
class Constant: NSObject {

    
    
}
