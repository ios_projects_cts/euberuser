//
//  ViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/13/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    // MARK:- btn_signup Action
    @IBAction func btn_signupAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    // MARK:- btn_login Action
    @IBAction func btn_loginAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    // MARK:- btn_forgot password Action
    @IBAction func btn_forgotPassAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"ForgetViewController") as! ForgetViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

