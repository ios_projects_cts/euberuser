//
//  HistoryViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    
    
    
    //MARK:- declare var,objects and outlets
    @IBOutlet weak var tbl_historylist: UITableView!
   var historyListArr = NSMutableArray()
      var activityIndicatorView: ActivityIndicatorView!
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "History"
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
   navigationController?.navigationBar.backItem?.title = ""
       
        self.callgetHistoryMethod()
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        
        

    }
    
    func callgetHistoryMethod()
    {
        
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "PassengerID": UserDefaults.standard.object(forKey: "passengerID")!
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetGetHistoryUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let status = result["status"] as! String
            if status == "success"
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // parse data from server
                    let venderArr=result["data"] as? [[String: Any]]
                    self.parseGetHistoryListData(dataArray: venderArr!)

                }
                else
                {
                    // display alert controller
                    self.displayAlertWithMessage(viewController:self, title: message, message:"")
                }
            }
            else
            {
                
            }
            
        }
    }
    
    
    //  MARK:- parseGetLocationListData
    func parseGetHistoryListData(dataArray:[[String: Any]])
    {
        historyListArr.removeAllObjects()
        
        let historyDict = NSMutableDictionary()
        for i in 0...dataArray.count-1
        {
            historyDict.setValue((dataArray[i]["PassengerID"] as? NSString), forKey: "PassengerID")
            historyDict.setValue((dataArray[i]["car_status"] as? NSString), forKey: "car_status")
            historyDict.setValue((dataArray[i]["drop_location"] as? NSString), forKey: "drop_location")
            historyDict.setValue((dataArray[i]["lat"] as? NSString), forKey: "lat")
            historyDict.setValue((dataArray[i]["lng"] as? NSString), forKey: "lng")
              historyDict.setValue((dataArray[i]["status"] as? NSString), forKey: "status")
               historyDict.setValue((dataArray[i]["Name"] as? NSString), forKey: "Name")
               historyDict.setValue((dataArray[i]["BookedTime"] as? NSString), forKey: "BookedTime")
            historyListArr.add(historyDict.mutableCopy() )
            
            
        }
        DispatchQueue.main.sync
            {
               
                tbl_historylist.reloadData()
                self.activityIndicatorView.stopAnimating()
        }
        
    }



    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return historyListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! HistoryTVCell
          let dict = historyListArr[indexPath.row] as? Dictionary<String,String>
        cell.lbl_status.text = dict?["status"]
          cell.lbl_dropLocation.text = dict?["drop_location"]
        cell.lbl_Name.text = dict?["Name"]
        cell.lbl_bookingTime.text = dict?["BookedTime"]
        return cell
        
    }
    
    
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
                   return 155
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
