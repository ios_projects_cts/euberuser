//
//  ConfirmBookingViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/14/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import GooglePlaces


// MARK:- Autocomplete delegate methods of google place api
extension ConfirmBookingViewController: GMSAutocompleteViewControllerDelegate,UITextFieldDelegate {
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // TODO: Handle the user's selection.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
        
        if locAuto == "pick"
        {
            txtf_pickAdd.text =  place.name + place.formattedAddress!
        }
        else
        {
            txtf_dropAdd.text =  place.name + place.formattedAddress!
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
        // TODO:User canceled the operation.
        if txtf_pickAdd.text == "" {
            txtf_pickAdd.text = self.userAddress
        }
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

class ConfirmBookingViewController: UIViewController {

      var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var txtf_dropAdd: CustomTextField!
    @IBOutlet weak var txtf_pickAdd: CustomTextField!
    @IBOutlet weak var view_searchBack: UIView!
    var userAddress = String()
    var pickupaddAddress = String()
    var current_latitude = String()
    var current_longitude = String()
    var TripClassTypesID = String()
     var locAuto = String()
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Confirm Booking"
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //MARK:- set effect of uiview for search
        navigationController?.navigationBar.backItem?.title = ""
//        if self.userAddress != ""
//        {
//             txtf_pickAdd.text = "  " + userAddress
//        }
        if self.pickupaddAddress == ""
        {
            txtf_pickAdd.text = userAddress
        }
        else
        {
            txtf_pickAdd.text = pickupaddAddress
        }

                self.view_searchBack.backgroundColor = UIColor.white
                self.view_searchBack.layer.cornerRadius = 3.0
                self.view_searchBack.layer.borderWidth = 2.0
                self.view_searchBack.layer.borderColor = UIColor.clear.cgColor
                self.view_searchBack.layer.shadowColor = UIColor.lightGray.cgColor
                self.view_searchBack.layer.shadowOpacity = 1.0
                self.view_searchBack.layer.shadowRadius = 1.0
                self.view_searchBack.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(3))
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtf_pickAdd
        {
            locAuto = "pick"
        }
        else
        {
            locAuto = "drop"
        }
        
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)

    }
    // MARK:- btn_cash Action
    @IBAction func btn_cashAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
      
    }
    // MARK:- btn_confirmBooking Action
    @IBAction func btn_confirmBookingAction(_ sender: Any)
    {
        
        if ((txtf_pickAdd.text?.characters.count)! > 0) && ((txtf_dropAdd.text?.characters.count)! > 0)
        {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())

        self.callBookCabMethod()
        }
        else
        {
            self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "Address Fields can't be empty")
        }
    }
    
    // MARK:- btn_fair estimate Action
    @IBAction func btn_fairEstimateAction(_ sender: Any)
    {
        
      if ((txtf_pickAdd.text?.characters.count)! > 0) && ((txtf_dropAdd.text?.characters.count)! > 0)
        {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        
        self.callgetPriceMethod()
        }
        else
        {
            self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "Address Fields can't be empty")
        }
    }
    
    
    // MARK:- Server call of getPriceMethod
    func callgetPriceMethod()
    {
        activityIndicatorView.startAnimating()
        let apiCall = JsonApi()
        let parameters = [
            
            "pickupLocation": txtf_pickAdd.text!,
            "dropLocation": txtf_dropAdd.text!
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetPriceUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    let data = result["data"] as! NSDictionary
                    
                    let max_price = String(describing: data["max_price"]!)
                    let min_price =  String(describing: data["min_price"]!)
                    DispatchQueue.main.async{
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self,title:"Estimated Price \n " ,message:min_price + "$ to " + max_price + "$ " + "for this location.")}
                    
                }
                else
                {
                    self.displayAlertWithMessage(viewController:self,title:"Estimated Price" ,message:message + "for this location.")
                }
            }
            else
            {
                
            }
        }
    }
    

    //MARK:- server call of get vednder location method
    func callBookCabMethod()
    {
        activityIndicatorView.startAnimating()
        
        let apiCall = JsonApi()
        let parameters = [
            
            "lat": current_latitude,
            "lng": current_longitude,
            "TripClassTypesID": TripClassTypesID,
            "drop_location": txtf_dropAdd.text!,
            "PassengerID": UserDefaults.standard.object(forKey:"passengerID")!,
            
           
            ]
         print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalbookCarUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                let data = result["data"] as! NSDictionary
                
                print(status)
                if success == "true"
                {
                  
                    DispatchQueue.main.async
                        {
                    
                    self.activityIndicatorView.stopAnimating()
                    let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Done", style: .default) { action in
                        UserDefaults.standard.set(data["booking_id"], forKey: "booking_id")
                            // call next view controller in main queue
                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
                            let vc  = storyboard.instantiateViewController(withIdentifier:"WaitingAcceptViewController") as! WaitingAcceptViewController
                            self.navigationController?.pushViewController(vc, animated: false)
                        

                    })
                    self.present(alert, animated: true)
                    }
                }
                else
                {
                    DispatchQueue.main.async
                    { self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                }
            }
            else
            {
                
            }
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
