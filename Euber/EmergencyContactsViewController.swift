//
//  EmergencyContactsViewController.swift
//  Euber
//
//  Created by chawtech solutions on 2/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit
import Contacts

class EmergencyContactsViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    
    
    
    //MARK:- declare var,objects and outlets
    @IBOutlet weak var tbl_contactlist: UITableView!
    var contactsArr = NSMutableArray()

    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        // set notification center addobservber
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Contacts"
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
      navigationController?.navigationBar.backItem?.title = ""
        
        self.findContacts()
    }
    
    
    //MARK:- btn cancel action
    @IBAction func btn_CancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func findContacts()  {
        
     contactsArr.removeAllObjects()
        
        let store = CNContactStore()
        
        let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        
        let fetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch as [CNKeyDescriptor])
        
        var contacts = [CNContact]()
        
        do {
           try! store.enumerateContacts(with: fetchRequest) { contact, stop in
                contacts.append(contact)
            
            
                let dict = NSMutableDictionary()
            
               // self.marrContactsName.addObject(contact.givenName + " " + contact.familyName)
            dict .setValue((contact.givenName + " " + contact.familyName), forKey: "name")
            
          
            /* Get only first mobile number */
            
            let MobNumVar = (contact.phoneNumbers[0].value ).value(forKey: "digits") as! String
            print(MobNumVar)
            dict .setValue(MobNumVar, forKey: "mobile")
            /* Get all mobile number */
            
            for ContctNumVar: CNLabeledValue in contact.phoneNumbers
            {
                let MobNumVar  = (ContctNumVar.value ).value(forKey: "digits") as? String
                print(MobNumVar!)
            }
            
            /* Get mobile number with mobile country code */
            
            for ContctNumVar: CNLabeledValue in contact.phoneNumbers
            {
                let FulMobNumVar  = ContctNumVar.value 
                let MccNamVar = FulMobNumVar.value(forKey: "countryCode") as? String
                let MobNumVar = FulMobNumVar.value(forKey: "digits") as? String
                
                print(MccNamVar!)
                print(MobNumVar!)
            }
            
            self.contactsArr.add(dict.mutableCopy())
            }
            
            print(contactsArr)
            tbl_contactlist.reloadData()
            
        }
            catch let error as Error {
                print(error.localizedDescription)
            }
            
        
            
        
        }
    

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! ContactsTVCell
          let dict = contactsArr[indexPath.row] as? Dictionary<String,String>
        
          cell.lbl_Name?.text = dict?["name"]
         cell.lbl_mobile?.text = dict?["mobile"]
        
        
        return cell
        
    }
    
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //               return 40
    //    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
