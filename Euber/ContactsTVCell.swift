//
//  ContactsTVCell.swift
//  Euber
//
//  Created by chawtech solutions on 2/23/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class ContactsTVCell: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var view_Back: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.view_Back.backgroundColor = UIColor.white
        self.view_Back.layer.cornerRadius = 5.0
        // self.view_taxiBack.layer.borderWidth = 2.0
        self.view_Back.layer.borderColor = UIColor.clear.cgColor
        self.view_Back.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_Back.layer.shadowOpacity = 1.0
        self.view_Back.layer.shadowRadius = 1.0
        self.view_Back.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(1))
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
