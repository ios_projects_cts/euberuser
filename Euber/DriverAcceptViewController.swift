//
//  DriverAcceptViewController.swift
//  Euber
//
//  Created by chawtech solutions on 3/1/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

class DriverAcceptViewController: UIViewController {
   var activityIndicatorView: ActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       
        self.callDriverBookingDetailMethod()
    }
    // MARK:- btn_cancel Action
    @IBAction func btn_cancelAction(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK:- btn_cancel Action
    @IBAction func btn_cancelBookingAction(_ sender: Any)
    {
        // call next view controller in main queue
        DispatchQueue.main.async {
            
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier: "CancelRideViewController") as! CancelRideViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }

    }
    // MARK:- btn_contact Action
    @IBAction func btn_contactAction(_ sender: Any)
    {
          self.callNumber(phoneNumber:"8738847483")
    }
    //MARK:- call number method
    private func callNumber(phoneNumber:String) {
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                // application.openURL(phoneCallURL as URL);
                application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
            }
        }
    }
    //MARK:- server call of get driver booking details method
    func callDriverBookingDetailMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Processing...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        activityIndicatorView.startAnimating()
        
        let apiCall = JsonApi()
        let parameters = [
            
            "booking_id": UserDefaults.standard.object(forKey:"booking_id")!,
        
            ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetBookingDetailsUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
              //  let data = result["data"] as! NSDictionary
                
                print(status)
                if success == "true"
                {
                    DispatchQueue.main.async
                    {
                            self.activityIndicatorView.stopAnimating()
                           
                    }
                }
                else
                {
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                }
            }
            else
            {
                
            }
        }
    }
    
    
  /*  //MARK:- server call of Cancel booking method
    func callCancelCabMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async
            {
        self.activityIndicatorView.startAnimating()
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "booking_id": UserDefaults.standard.object(forKey:"booking_id")!,
            
            ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalcancelBookingUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                
                print(status)
                if success == "true"
                {
                    
                    DispatchQueue.main.async
                        {
                            
                            
                            self.activityIndicatorView.stopAnimating()
                            
                            // create the alert
                            let alert = UIAlertController(title:"Message", message:message, preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                
//                                for controller: UIViewController in (self.navigationController?.viewControllers)! {
//                                    if (controller is HomeViewController) {
//                                        _ =  self.navigationController?.popToViewController(controller, animated: false)
//                                    }
//                                }
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                    }
                }
                else
                {
                    DispatchQueue.main.async
                        { self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title: "Message", message: message)
                    }
                }
            }
            else
            {
                
            }
        }
        
    }
    */

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
